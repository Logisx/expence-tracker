require('dotenv').config();
const mysql = require('mysql2/promise');

try {
    db = mysql.createPool({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    });
    console.log('Database connected');
} catch (error) {
    console.error('Failed to create a database pool: ', error);
    throw error;
}

module.exports = db;