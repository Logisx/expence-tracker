// test/AuthenticationController.test.js

const chai = require('chai');
const sinon = require('sinon');
const mocha = require('mocha')
const AuthenticationController = require('../src/controllers/authenticationController');
const AuthenticationModel = require('../src/models/authenticationModel');

const expect = chai.expect;

describe('AuthenticationController', () => {
  afterEach(() => {
    sinon.restore();
  });

  // it('should return users array', async () => {
  //   const usersStub = sinon.stub(AuthenticationModel, 'queryGetUsers').resolves([]);
  //   const users = await AuthenticationController.getUsers();
  //   expect(users).to.be.an('array');
  // });

  describe('validateCreatingCredentials', () => {
    it('should throw an error if the name field is empty', () => {
      expect(() => AuthenticationController.validateCreatingCredentials('', 'username', 'password')).to.throw('Name field cannot be empty');
    });

    it('should throw an error if the password is less than 8 characters long', () => {
      expect(() => AuthenticationController.validateCreatingCredentials('name', 'username', 'pass')).to.throw('Password should be at least 8 characters long');
    });

    it('should throw an error if the password is more than 30 characters long', () => {
      const longPassword = 'a'.repeat(31);
      expect(() => AuthenticationController.validateCreatingCredentials('name', 'username', longPassword)).to.throw('Password should be less than 30 characters');
    });

    it('should throw an error if the username field is empty', () => {
      expect(() => AuthenticationController.validateCreatingCredentials('name', '', 'password')).to.throw('Username field cannot be empty');
    });

    it('should return true for valid input', () => {
      const result = AuthenticationController.validateCreatingCredentials('name', 'username', 'password');
      expect(result).to.be.true;
    });
  });
  describe('createUser', () => {
    it('should resolve successfully if the SQL query completes', async () => {
      const queryCreateUserStub = sinon.stub(AuthenticationModel, 'queryCreateUser').resolves();

      await AuthenticationController.createUser({
        name: 'Alex Smith',
        login: 'alexsmith2',
        password: 'password123',
      });
      expect(queryCreateUserStub.calledOnce).to.be.true;
    });

    it('should reject if the SQL query fails', async () => {
      const error = new Error('Database query failed');
      const queryCreateUserStub = sinon.stub(AuthenticationModel, 'queryCreateUser').rejects(error);

      const consoleErrorStub = sinon.stub(console, 'error');

      await AuthenticationController.createUser({
        name: 'John Doe',
        login: 'john.doe',
        password: 'password123',
      });

      expect(queryCreateUserStub.calledOnce).to.be.true;
      expect(consoleErrorStub.calledOnce).to.be.true;
      expect(consoleErrorStub.calledWith('Database query failed:', error)).to.be.true;
    });
  });
  
});