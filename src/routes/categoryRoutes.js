const path = require('path')

const express = require('express');
const router = express.Router();
const logger = require('../controllers/log.controller');

// routes.js
/**
 * Controller for managing categories.
 * @type {import('../controllers/categoryController')}
 */
const CategoryController = require('../controllers/categoryController');


router.get('/list', async (req, res) => {
    if (req.session.user) {
        try {
            /**
             * Retrieves the categories from the CategoryController.
             * @returns {Array} An array of categories.
             */
            const categories = await CategoryController.getCategories();
            res.json({ data: categories });
        } catch (error) {
            console.error('Error occurred:', error);
            return res.status(500).json({ error: 'Internal server error' });
        }
    } else {
        res.redirect('./');
    }
});

module.exports = router;
