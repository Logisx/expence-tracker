const logger = require('../controllers/log.controller');

/**
 * Middleware to log entry information.
 * 
 * @param {import("express").Request} req - The Express request object.
 * @param {import("express").Response} res - The Express response object.
 * @param {import("express").NextFunction} next - The next middleware function.
 */
const entryMW = (req, res, next) => {
    // Create a string representing the endpoint by combining the base URL and path
    const endpoint = req.baseUrl + req.path;

    // Log the entry information
    logger.info(`Entry ${endpoint}`);

    // Proceed to the next middleware in the chain
    next();
};

module.exports = entryMW;