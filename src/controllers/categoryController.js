// expenseController.js

const CategoryModel = require('../models/categoryModel');

/**
 * Controller class for managing categories.
 */
class CategoryController {
  
  /**
   * Retrieves the list of categories from the database.
   * @returns {Promise<string[]>} The array of category names.
   */
  static async getCategories() {
    try {
      const categories = await CategoryModel.queryCategories();
      const categoryNames = [];
      categories[0].forEach(element => {
        categoryNames.push(element.name);
      });
      console.log(categoryNames);
      return categoryNames;
    } catch (err) {
      console.error('Database query failed:', err);
    }
  }
}

module.exports = CategoryController;
