// expenseModel.js

/**
 * Represents the Expense Model.
 * @class
 */
class ExpenseModel {

  /**
   * Configuration object for the category model.
   * @type {Object}
   * @property {Object} db - The database configuration.
   */
  static config = {
    db: require('../../config/db'),
  }

  /**
   * Queries the categories from the database.
   * @returns {Promise<Array<Object>>} The categories retrieved from the database.
   */
  static async queryCategories() {
    const categories = await db.query(
      `SELECT id, name
      FROM category
      ORDER BY name ASC`,
    );
    return categories;
  }

  /**
   * Queries categories by name.
   * @param {string} name - The name to search for.
   * @returns {Promise<Array<Object>>} - The categories matching the name.
   */
  static async queryCategoriesByName(name) {
    const categories = await db.query(
      `SELECT id, name
      FROM category
      WHERE name LIKE "%${name}%"`
    );
    console.log('Queried categories:', categories);
    return categories[0];
  }

  /**
   * Creates a new category with the given name.
   * @param {string} name - The name of the category.
   * @returns {number} - The ID of the newly created category.
   */
  static async create(name) {

    const result = await db.query(
      `INSERT INTO category (name) VALUES (?)`,
      [name]
    );
    console.log('Created category:', result);

    return result[0].insertId;
  }
}

module.exports = ExpenseModel;