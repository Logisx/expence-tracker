// expenseModel.js

/**
 * Represents a model for managing expenses.
 */
class ExpenseModel {

  /**
   * Configuration object for the expense model.
   * @type {Object}
   * @property {Object} db - The database configuration.
   */
  static config = {
    db: require('../../config/db'),
  }

  /**
   * Queries expenses for a specific user.
   * @param {number} userId - The ID of the user.
   * @returns {Promise<Array>} - A promise that resolves to an array of expenses.
   */
  static async queryExpenses(userId) {
    const expenses = await db.query(
      `SELECT e.id AS expense_id, amount, description, c.name AS category, DATE(\`date\`) as date
      FROM expense AS e
      LEFT JOIN category AS c ON c.id = e.category_id
      LEFT JOIN user AS u ON u.id = e.user_id
      WHERE u.id = ?
      ORDER BY date DESC, c.id DESC`,
      [userId]
    );
    return expenses[0];
  }

  /**
   * Creates a new expense record in the database.
   * @param {Object} expenseData - The data for the expense.
   * @param {number} expenseData.amount - The amount of the expense.
   * @param {string} expenseData.description - The description of the expense.
   * @param {number} expenseData.category_id - The ID of the category associated with the expense.
   * @param {number} expenseData.user_id - The ID of the user associated with the expense.
   * @param {string} expenseData.date - The date of the expense.
   * @returns {number} - The ID of the newly created expense record.
   */
  static async create(expenseData) {
    const { amount, description, category_id, user_id, date } = expenseData;

    const result = await db.query(
      `INSERT INTO expense (amount, description, category_id, user_id, date) VALUES (?, ?, ?, ?, ?)`,
      [amount, description, category_id, user_id, date]
    );
    console.log('Created expense:', result);

    return result[0].insertId;
  }
}

module.exports = ExpenseModel;